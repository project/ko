Drupal Korean Translation Project

Current Target: Drupal 6.4 

Sponsors:
 
  * Drupal Korea: http://drupal.kldp.org
  * KLDP - Korean Linux Documentation Project: http://www.kldp.org

Contributors:

  * John Hwang (co-maintainer)
  * ChulKi Lee (co-maintainer)
  * jachin
  * naomi1976

Background Information:

After many individual attempts to translate drupal to Korean, individuals from the KLDP community joined forces to work on the an official Korean Drupal Translation project.  At the same time, the Drupal Korea project was born to facilitate and help coordinate the translation efforts.  With many high profile F/OSS projects showing interest in converting to Drupal, Drupal Korea aims to provide a complete Korean distribution targetting Korean Internet users.  The goal is to promote/market Drupal within the Korean online community, begin development of modules & themes to support the interests/needs of the korean community, and maintain a localized version of Drupal packaged with approapriate modules and themes.  All improvements and development efforts will be given back to the Drupal community at large.

============
 CHANGELOG
============

2008-12-11
================================================================================
* ko.po - 100% translated version targetting Drupal 6.4 - Fixed a couple \n ending issues

2006-01-22
================================================================================
* forum-module.po
* node-module.po
* page-module.po
* taxonomy-module.po
* user-module.po

Adding new modules and updates to previously committed modules

2006-01-11
================================================================================
* aggregator-module.po
* archive-module.po
* block-module.po
* blogapi-module.po
* blog-module.po
* book.module.po
* comment-module.po
* common-inc.po
* general.po
* locale-inc.po
* locale-module.po
* search-module.po
* README.txt - updated

Crrected the ISO 639 Code... Moving/Renaming kr to ko


2006-01-11
================================================================================
* aggregator-module.po
* archive-module.po
* block-module.po
* blogapi-module.po
* book-module.po
* locale-inc.po
* locale-module.po
* search-module.po

Update existed translation. It's from 4.6 pot, so some file has unused string or untranslation string. We'll solve it soon.

2006-01-10
================================================================================
* blog-module.po
* comment-module.po
* common-inc.po
* general.po
* README.txt

Initial Import of core modules and preliminary translation work/updates
